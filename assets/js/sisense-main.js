//	Load the Sisense.js Library
sisenseApp.init = function(webpage){

	//	Get the url of the Sisense.js library
	var url = sisenseApp.settings.serverUrl + '/js/sisense.js';

	//	Create a new script tag for Sisense.js, if it's not loaded already
	if (!document.getElementById(sisenseApp.settings.scriptId)){

		//	No script existing, add a new one
		var el = document.createElement('script');
	        el.id = sisenseApp.settings.scriptId;
	        el.src = url;
	        el.onload = function(script){
	            console.log('Sisense.js loaded');
	            sisenseApp.loadDashboard(webpage);
	        };

	    //	Append this script tag to the web page
	    document.body.append(el);
	} else {
		//	Script already loaded, just init the dashboard
		sisenseApp.loadDashboard(webpage);
	}
}

//	Function to Instantiate the Sisense web app	
sisenseApp.getApp = function(callback,param) {

	//	Check to see if Sisense.js has loaded at all
	if (typeof Sisense == "function") {

		//	Has the application been connected?
		if (!sisenseApp.savedInstance) {

			//	Nope, connect to the Sisense server
			return checkLogin().then(function(isAuthenticated){

				//	Is the user authenticated already?
				if (true){

					//	Yep, the user is authenticated, start the connection
					Sisense.connect(sisenseApp.settings.serverUrl,false).then(function(app){		
						
						//	Save the instance
						sisenseApp.savedInstance = app;

						//	Check for window resizes
						var resizeTimer;
						$(window).on('resize', function(e) {
						  clearTimeout(resizeTimer);
						  resizeTimer = setTimeout(function() {
							// Resizing has "stopped"
							sisenseApp.loadDashboard(param);
						  }, 250);
						});

						//	Run any callback functions
						if (callback) {
							setTimeout(function(){
								callback(param);
							},500);
						}
					}, function(err){
						debugger
					});
				} else {
					//	No, user is not authenticated so redirect them to the login page
					handleUnauthenticated();
				}
			})
		} else {		
			//	Already loaded, return the instance
			return sisenseApp.savedInstance;	
		}	
	} else {
		window.location.reload();
	}
};

//	Function to render a widget
sisenseApp.renderWidget = function(widget, config){

	//	Get the widget's configg
	var widgetConfig = config.widgets[widget.$$model.oid];

	//	Find the div container
	var container = document.getElementById(widgetConfig.divId);

	//	The widget title container should either the "card-header" class, or a custom div
	var titleEl = widgetConfig.title.divId ? $('#' + widgetConfig.title.divId) : $('div.card-header', container);

	//	The widget body container should either the "card-body" class, or directly in the specified div
	var widgetEl = widgetConfig.nonStandard ? $(container) : $('div.card-content', container);

	//	Clear the widget's header and body
	$(titleEl, container).empty();
	$(widgetEl, container).empty();

	//	Should we show the title?
	var shouldShow = (widgetConfig.title.enabled) && (widget.title.length > 0 || widgetConfig.title.customText);
	if (shouldShow){

		//	Check for an override title, otherwise use the widget's title
		var titleText = widgetConfig.title.customText ? widgetConfig.title.customText : widget.title;

		//	Display the widget's title
		titleEl.css('display','block');
		titleEl.text(titleText);

		//	Set the color, if specified
		if (widgetConfig.title.color) {
			titleEl.attr('data-background-color', widgetConfig.title.color);
		}
	} else {

		//	Hide the title bar
		titleEl.css('display','none');
	}

	//	Define inline styling
	for (attr in widgetConfig.css){

		//	Double check the attribute exists
		if (widgetConfig.css.hasOwnProperty(attr)){

			//	Set the CSS property
			widgetEl.css(attr, widgetConfig.css[attr])
		}
	}

	//	Tell Sisense.js where to render the widget
	widget.container = widgetEl[0];

	//	Start loading the widget
	widget.refresh();

	//	Handle widget animation
	if (widgetConfig.animation){
		animateWidget(widget,widgetEl,widgetConfig);
	}

	//	Apply any CSS fixed
	fixCss(widget,widgetEl);
}

//	Function to load a dashboard
sisenseApp.loadDashboard = function(webpage) {

	//	Get the config for this dashboard from sisense-config.js
	var config = sisenseApp.dashboards[webpage];
	
	//	Get the app
	var app = sisenseApp.getApp(sisenseApp.loadDashboard,webpage);

	//	Need both the Sisense web application and a matching config, in order to run
	if (app && app.dashboards && config){
			
		//	Mark as the active dashboard
		sisenseApp.activeDashboardId = webpage;		

		//	Check to see if the dashboard has been loaded already
		var existingDashboard = $.grep(app.dashboards.$$dashboards, function(existingDashboard) { 
			return existingDashboard.$$model.desc==webpage; 
		})[0];	

		
		if (existingDashboard) {

			//	Redraw the dashboard
			sisenseApp.activeDashboard.$$model.redraw();

			//	Is there a filter container?
			var myFilters = sisenseApp.dashboards[webpage] ? sisenseApp.dashboards[webpage].filters : null;
			if (myFilters && myFilters.loaded) {

				//	Need to set a height for the container
				var filterHeight = $(window).height()> 100 ? $(window).height() - 75 : $(window).height(),
					filterContainer = document.getElementById(myFilters.divId);					
				$(filterContainer).css('height',filterHeight + 'px');
			}
			
		} else {

			//	Create a new dashboard from Sisense
			var newDashboard = new Dashboard();

			//	Add the dashboard to the Sisense application
			app.dashboards.add(newDashboard);

			//	Save a reference to this dashboard
			newDashboard.$$model.desc = webpage;

			//	Make this the active dashboard
			sisenseApp.activeDashboard = newDashboard;

			//	Loop through all widgets to add
			for (id in config.widgets) {

				//	Double check the widget exists in the config
				if (config.widgets.hasOwnProperty(id)) {

					//	Asynchronously load and render the widget
					newDashboard.widgets.load(id)
						.then(function(widget) {
							sisenseApp.renderWidget(widget, config);
						})
				}
			}

			//	Handle standard filters panel
			var canShowFilters = config.filters && config.filters.show;
			if (canShowFilters){

				//	Get filters and datasource from a dashboard
				var filterDashboardId = config.filters.dashboardId;

				//	Figure out the REST API call to get the dashboard's filters
				var url = sisenseApp.settings.serverUrl + '/api/v1/dashboards/' + filterDashboardId + '?fields=oid,title,datasource,filters,defaultFilters';

				//	Make an API call to get the filters from an existing dashboard
				app.$$http.get(url)
					.then(function(response){

						//	Set the data source for the filters
						newDashboard.$$model.datasource = response.data.datasource;

						//	Set the title of the dashboard
						newDashboard.$$model.title = response.data.title;

						//	Check for any default filters
						var dashboardFilters = response.data.defaultFilters ? response.data.defaultFilters : response.data.filters;

						//	Loop through each filter
						dashboardFilters.forEach(function(filter){

							//	Add in the filter
							sisenseApp.filters.setFilter(filter.jaql,false);
						})

						//	Refresh the dashboard, to include these filters
						newDashboard.refresh();

						//	Find the dashboard container
						var filterContainer = document.getElementById(config.filters.divId);

						//	Need to set a dynamic height for the container
						var filterHeight = $(window).height()> 100 ? $(window).height() - 75 : $(window).height();
						$(filterContainer).css('height',filterHeight + 'px');

						//	Load the filters
						newDashboard.renderFilters(filterContainer);
					})
			}

			//	Handle custom filters
			var canShowCustomFilters = config.customFilters && config.customFilters.datasource && config.customFilters.filters.length>0;
			if (canShowCustomFilters){

				//	Set the data source for the filters
				newDashboard.$$model.datasource = config.customFilters.datasource;

				//	Loop through the filter configs, and add them to the webpage
				config.customFilters.filters.forEach(createFilter);
			}

			//	Handle the dashboard title
			var canShowTitle = config.settings && config.settings.titleDiv;
			if (canShowTitle){
				$('#' + config.settings.titleDiv).text(config.settings.titleText);
			}
		}
	}
}

//	Group the filter related functions together
sisenseApp.filters = {
	//	Function to set a filter based on JAQL
	setFilter: function(jaql, autoRefresh) {
		//	Get the active dashboard, only run if sisense is loaded
		var dashboard = sisenseApp.activeDashboard;
		if (dashboard) {
			if (jaql) {			
				//	Create the filter jaql
				var filterJaql = {
					jaql: jaql
				};

				//	Create the filter options
				var filterOptions = {
					save:false, 
					refresh:autoRefresh, 
					unionIfSameDimensionAndSameType:false
				};

				//	Set the new filter
				sisenseApp.activeDashboard.$$model.filters.update(filterJaql,filterOptions);
			}
		}
	},
	//	Function to clear all filters on a dashboard
	clear: function(jaql) {

		//	Get the active dashboard, only run if sisense is loaded
		var dashboard = sisenseApp.activeDashboard;
		if (dashboard) {
			
			//	Set the new filter		
			dashboard.$$model.filters.clear()

			//	Refresh the dashboard
			dashboard.refresh();	
		}
	},
	//	Function to search a dimension
	search: function(dim,searchText,callback,params) {

		//	Get the start date first, then the end date	
		var metadata = [{
			"jaql": {
				"dim": dim,
				"datatype": "text",
				"filter": {
					"contains": searchText
				}
			}
		}];

	  	//	Run the query, and run the callback with the parameters		
		sisenseApp.data.$queryData(metadata).then(function(response){  		
	  		return callback(response,params);
	  	});
	},
	//	Function to a min and max date from a dimension
	getDateRange: function(dim,callback,param) {

		//	Get the start date first, then the end date	
		var metadata = [
		    {
				"jaql": 
				{
					"dim": dim, 
					"datatype": "datetime",
					"level": "days",
					"agg": "min"        
				}
		    },
		    {
				"jaql": 
				{
					"dim": dim, 
					"datatype": "datetime",
					"level": "days",
					"agg": "max"        
				}
		    }
		];

		//	Define options for the query
		var options = [
			{
				'key': 'count',
				'value': 1
			},
			{
				'key': 'offset',
				'value': 0
			}
		];

	  	//	Function to parse the data and make the original callback
	  	var finished = function(result){

	  		//	Make sure the API call was successful  		
	  		if (result.status == 200) {

	  			//	Save the dates returned
	  			var data = {
	  				'startDate': moment(result.data.values[0].data),
	  				'endDate': moment(result.data.values[1].data)
	  			};

			  	return data;
	  		}  		
	  	}

	  	//	Run the query, and run the callback with the parameters
	  	sisenseApp.data.$queryData(metadata,options).then(finished).then(function(defaultDateRange){
	  		return callback(defaultDateRange,param);
	  	});
	}
}

//	Define the data related functions
sisenseApp.data = {
	//	Function to make a jaql query against elasticube
	$queryData: function(metadata, options){

		var canRunQuery = (typeof sisenseApp.activeDashboard !== "undefined") && (typeof sisenseApp.savedInstance !== "undefined");
		if (canRunQuery) {

			//	Init the variables for the API call
			var datasource = sisenseApp.activeDashboard.datasource,
				url = sisenseApp.settings.serverUrl + "/api/datasources/" + datasource.title + "/jaql",
				data = {
					"datasource": datasource,				
					"metadata": metadata
				},
				config = {
					url: url,
					contentType: "application/json"
				}
				http = sisenseApp.savedInstance.$$http;

			//	Loop through the options, and add to payload
			if (options) {
				for (var i=0; i<options.length; i++) {
					var thisOption = options[i];
					data[thisOption.key] = thisOption.value;
				}
			}

			//	Make the API call
			return http.post(url,data);
		}
	},
	// 	Function to get a list of members
	getMembers: function(dims, callback, params) {

		//	Get the start date first, then the end date	
		var metadata = [];

		//	Loop through each dimension and add to the list
		for(var i=0; i<dims.length; i++){

			//	Create a jaql object for this dim
			var jaql = {
				"jaql": {
					"dim": dims[i],
					"datatype": "text"				
				}
			}

			//	Push to the array
			metadata.push(jaql);
		}	

		//	return the callback function
		sisenseApp.data.$queryData(metadata).then(function(response){
			return callback(response,params);
		})
	},
	getDateRange: function(dim, callback, params) {

		//	What level do we want to query for (days [default], weeks, months, quarters, years)?
		var queryLevel = params[0] ? params[0] : 'days';

		//	Get the start date first, then the end date	
		var metadata = [
		    {
				"jaql": 
				{
					"dim": dim, 
					"datatype": "datetime",
					"level": queryLevel,
					"agg": "min"        
				}
		    },
		    {
				"jaql": 
				{
					"dim": dim, 
					"datatype": "datetime",
					"level": queryLevel,
					"agg": "max"        
				}
		    }
		];

		//	Define options for the query
		var options = [
			{
				'key': 'count',
				'value': 1
			},
			{
				'key': 'offset',
				'value': 0
			}
		];

	  	//	Function to parse the data and make the original callback
	  	var finished = function(result){

	  		//	Make sure the API call was successful  		
	  		if (result.status == 200) {

	  			//	Save the dates returned
	  			var data = {
	  				'startDate': moment(result.data.values[0].data),
	  				'endDate': moment(result.data.values[1].data),
	  				'level': queryLevel
	  			};

			  	return data;
	  		}  		
	  	}

	  	//	Run the query, and run the callback with the parameters
	  	sisenseApp.data.$queryData(metadata,options).then(finished).then(function(defaultDateRange){
	  		return callback(defaultDateRange,params);
	  	});
	}
}

/*	Utility functions 	*/

//	Function to check and see if the user is logged in
function checkLogin(){

	//	API call url for verifying authentication
	var url = sisenseApp.settings.serverUrl + sisenseApp.settings.testApiEndpoint;

	var settings = {
		'url': url,
		'dataType': 'json',
		'crossDomain': true,
		'beforeSend': function(xhr){
	      xhr.withCredentials = true;
	    }
	}

	//	If the api call is successfull, return true... otherwise return false
	return $.ajax(settings).then(
		function(response){ 
			return true; }, 
		function(response){ 
			return false; }
	);
}

//	Function to handle what to do if the user is not logged in
function handleUnauthenticated(){

	//	Not authenticated, redirect to login page
	var redirectUrl = sisenseApp.settings.serverUrl + '/app/account#/login?src=resources/embed/';

	//	Perform redirection
	window.location.href = redirectUrl;
}

//	Function to create a custom HTML/JavaScript filter
function createFilter(config){

	//	Set the label
	$('#' + config.labelDivId).text(config.title);

	//	Find the filter div container
	var element = $('#' + config.divId);

	//	Check the filter type
	if (config.type == "dropdown") {

		//	Function to create an individual menu item
		function initMenuItem(text){

			//	Create the HTML for a single member
			var member = '<li><a href="#" class="btn-' + config.color + '">' + text + '</a></li>';
			if (text == config.noSelectionText){
				member += '<li class="divider"></li>';
			}

			return member;
		}

		//	Function to handle menu selection
		function menuItemClicked(event){

			//	Get the selection
			var selection = $(this).text();

			//	Define the base filter option
			var filter = {
				'datatype': config.datatype,
				'dim': config.dimension,
				'title': config.title
			}

			//	Figure out if the No Selection option was seleted
			var noSelection = (selection === config.noSelectionText);
			if (!noSelection){
				filter.filter = {
					'members': [selection],
					'explicit': false,
					'multiSelection': true
				}
			} else {
				filter.filter = {
					'all': true,
					'explicit': false,
					'multiSelection': true
				}
			}
			//	Set the filter selection via JavaScript API
			sisenseApp.filters.setFilter(filter,true);

			//	Change the menu label
			var menuLabel =  $('a.dropdown-toggle',element);
			$(menuLabel).text(selection)

			console.log('Setting ' + filter.title + ' filter to ' + selection);
			
		}

		//	Dropdown menu
		var initDropdown = function(response,params){

			//	Init the parameters
			var element = params[0],
				config = params[1],
				data = response.data,
				labelEl = $('a.dropdown-toggle',element);

			//	Get the unordered list that contains our filter selections
			var listEl = $('ul.dropdown-menu',element);

			//	Remove/clear any placeholder elements
			$(listEl).empty();
			$(labelEl).text('');

			//	Make sure there wasn't a query error
			if (data && !data.error){

				//	Set the label
				$(labelEl).text(config.noSelectionText);

				//	Add in the no selection option
				$(listEl).append(initMenuItem(config.noSelectionText));

				//	Loop through each member
				data.values.forEach(function(member){
					$(listEl).append(initMenuItem(member[0].text));
				})

				//	Add click handlers
				$('a',listEl).on('click',menuItemClicked);

			} else {

				//	Note the error in the console.
				console.log("Error building filter for " + config.title + ", " + (data.error ? data.error : response.statusText));
			}
		}

		//	Set the background color
		$('a.btn',element).addClass('btn-' + config.color);

		//	Use the REST API to get a list of possible filter selections, pass the results to the initDropdown function
		sisenseApp.data.getMembers([config.dimension],initDropdown,[element,config]);

	} else if (config.type == "toggle") {

		//	Set background color
		$(element).addClass('sisensetoggle-' + config.color);

		//	Handle Toggle menu changes
		element.on('change',function(){

			//	Define the base filter option
			var filter = {
				'datatype': config.datatype,
				'dim': config.dimension,
				'title': config.title
			}

			//	Figure out if the checkbox is enabled or not 
			var enabled = $('input[type="checkbox"]',this).prop('checked');

			if (enabled){
				filter.filter = {
					'members': config.members,
					'explicit': false,
					'multiSelection': true
				}
			} else {
				filter.filter = {
					'all': true,
					'explicit': false,
					'multiSelection': true
				}
			}
			//	Set the filter selection via JavaScript API
			sisenseApp.filters.setFilter(filter,true);

			console.log('Setting ' + filter.title + ' filter as ' + (enabled ? 'enabled' : 'disabled'));
		})

	} else if (config.type == "slider") {

		//	Slider control
		function initSlider(range, params){

			//  Define the 
		    var sliderElement = document.getElementById(config.divId),
		    	labelElement = $('#' + config.labelDivId);

		    var startTime = parseInt(range.startDate.format('x')),
		    	endTime = parseInt(range.endDate.format('x'));

		    //	Initialize the slider, with options 	(allow only days as selections)
		    noUiSlider.create(sliderElement, {
		        start: [ startTime, endTime ],
		        connect: true,
		        step: 24 * 60 * 60 * 1000,
		        range: {
		            min:  startTime,
		            max:  endTime
		        }
		    });

		    //	Get the formatted selection
	    	var newLabel = config.title + ": " + formatTimestamp(startTime,config.momentFormatString) + ' to ' + formatTimestamp(endTime,config.momentFormatString);

	    	//	Update the label
			labelElement.text(newLabel);

			//	Style the slider
			var connect = sliderElement.querySelectorAll('.noUi-connect');
			connect.forEach(function(thisConnect){
				thisConnect.classList.add('slider-connect-' + config.color);
			})

			var handles = sliderElement.querySelectorAll('.noUi-handle');
			handles.forEach(function(handle){
				handle.classList.add('slider-handles-' + config.color);	
			})
			
			//	Add event handler for updating the labels
			sliderElement.noUiSlider.on('update', function( values, handle ) {

				//	Get the formatted selection
		    	var newLabel = config.title + ": " + formatTimestamp(values[0],config.momentFormatString) + ' to ' + formatTimestamp(values[1],config.momentFormatString);

		    	//	Update the label
				labelElement.text(newLabel);

			})

		    //	Add event handler for when the value is changed
		    sliderElement.noUiSlider.on('set', function( values, handle ) {

		    	//	Get the formatted selection
		    	var newLabel = config.title + ": " + formatTimestamp(values[0],config.momentFormatString) + ' to ' + formatTimestamp(values[1],config.momentFormatString);

		    	//	Update the label (just in case)
				labelElement.text(newLabel);

				//	Figure out the Sisense way of formatting those dates
				var fromDate = formatTimestamp(values[0]),
					toDate = formatTimestamp(values[1]);

				//	Define the base filter option
				var filter = {
					'datatype': config.datatype,
					'dim': config.dimension,
					'level': config.level,
					'title': config.title,
					'filter': {
						'from': fromDate,
						'to': toDate
					}
				}

				//	Set the filter
				sisenseApp.filters.setFilter(filter,true);

				console.log('Setting ' + filter.title + ' filter as ' + fromDate + ' to ' + toDate);
			});
		}

		//	Use the REST API to get a list of possible filter selections, pass the results to the initDropdown function
		sisenseApp.data.getDateRange(config.dimension,initSlider,[config.level]);
	}
}

//	Function to format timestamps
function formatTimestamp(timestamp,formatString){

	//	Look for a supplied Moment.js format string, use the Sisense as default
	var myFormat = formatString ? formatString : sisenseApp.settings.dateFormatString;

	//	Make sure the timestamp is an integer
	//var ts = parseInt(timestamp);

	//	Create a moment object
	var m = moment(timestamp,'x');

	//	Return text, using the supplied Moment.js format string
	return m.format(myFormat);
}

//	Function to recursively wait until a widget has finished rendering
function animateWidget(widget,widgetEl,widgetConfig){

	//	Is the widget still rendering?
	if (widget.$$model.refreshing){
		//	Yes, wait 0.25 seconds
		setTimeout(function(){
			animateWidget(widget,widgetEl,widgetConfig);
		},250);
	} else {
		//	No, find the card element
		var cardEl = widgetEl.closest('.card');

		//	Check for a delay
		var delay = widgetConfig.animationDelay ? widgetConfig.animationDelay : 0;

		//	Only run if the card is hidden (once)
		if (cardEl.hasClass(sisenseApp.settings.hiddenCardClass)){

			//	Run, but after any potential delays
			setTimeout( function(){
				//	Remove the hidden class
				cardEl.removeClass(sisenseApp.settings.hiddenCardClass);

				//	Add the animation css
				cardEl.addClass(widgetConfig.animation);	
			},delay);
		}
	}
}

//	Define the function to fix any widgets
function fixCss(widget, container){

	//	Check the widget type
	if (widget.$$model.type == "pivot"){

		//	Function for fixing pivot table's css
		function pivotCss(args){
			$('table#pivot_', container).css('table-layout', 'inherit');
			$('td', container).css('width', 'auto');
			$('td', container).css('min-width', '100px');
			$('pivot div.wrapper', container).css('height','100%');
		}

		//	Make sure function runs, on "domready" event
		widget.$$model.on('domready', pivotCss );

	}
}