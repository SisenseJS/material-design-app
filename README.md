#	Material Design Sample App

This project is a sample website to show how to embed using Sisense.js.  It leverages a free web framework by www.creative-time.com, more information on this can be found at this link: https://www.creative-tim.com/product/material-dashboard

This sample is a client-side only application, so there is no authentication.  The server used in these example webpages is a Sisense-owned demo server, so the workflow is to authenticate to the sample server and then open up this website through a link.

This sample was developed on Sisense version 6.7.1, but should be backwards compatible with previous version.

All Sisense related code is contained within the following files
	
	/assets/js/sisense-config.js 
	/assets/js/sisense-main.js 

An important distinction is that this implementation is just a sample of how sisense.js can be implemented.  These files would be structured much differently, if being part of a node.js or angular application.  The important parts to learn from this example are the steps for loading/creating dashboards.

__Step 1: Load the Sisense.js library__ - This sample site uses javascript to lazily load the sisense.js file, as that's how most angular/react/etc application will work.  In those cases, the you create a module and that model may be loaded at different points within the lifecycle of the application.  The lazy-loading approach ensures the library always gets loaded (initially) whenever sisense content is being displayed.  This is also typically where authentication will happen.  In order to work with Sisense.js, you will need to have Single Sign On enabled through your application.  More information on SSO can be found here: https://developer.sisense.com/pages/viewpage.action?pageId=1409378

__Step 2: Load/Create a dashboard__ - With Sisense.js, you have two approaches in terms of how you display content.  You can either load an existing dashboard (with all it's widget and filters), or you can create a brand new dashboard and add the widgets/filters on the fly.  If you've already build a dashboard in Sisense Web and want to display all the widgets/filters from it, the easy option is to just load the whole dashboard, loop through the widgets, and specify where to render them.  This sample application takes a different approach, where a new dashboard is created when the page opens and it reads what widgets/filters to load from a config file.  The advantage here is that only specific widgets that are being used are ever loaded into the web page, so it can be slightly more efficient.

__Step 3: Add Filters__ - Sisense.js lets you embed our native filter panel into your application, but you also have the option to create your own filters and then just leverage Sisense APIs to tie them to your dashboard.  This lets you build/repurpose your own components, and have them hook into Sisense dashboards.  For some examples of this, check out the Impressions Dashboard (/examples/impressions.html)

__Optional: Communicating with iframe embedded Sisense__ - When you choose to embed Sisense dashboards through iframes, you must consider the communication channel between the parent window and iframe window.  By default, most browsers will block this communication as it qualifies as Cross Site Scripting.  The recommended approach to communicate between Sisense in an iframe and a parent website is to use _postMessage_.  In order to do this, you need to have message senders and listeners on both the Sisense Web App side and your custom application side.  Check out the plugin at /SisenseWebPlugins/EmbedHelper for a sample of how to send messages from SisenseWeb to a parent application.  The file at /examples/iframe.html contains some JavaScript at the bottom that shows how to create event listeners (use jQuery to assign the event lister) and catch any messages that bubble up from the iframe.